
function call(elem) {
    var data = elem.serialize();
    $.ajax({
        type: 'POST',
        url: '/ajax.php',
        data: data,
        success: function (request) {
            console.log(request);
            alert(request);
        },
        error: function (xhr, str) {
            alert('Возникла ошибка: ' + xhr.responseCode);
        }
    });
}

$(".form").on("submit", function(){
    call($(this));
    return false;
});